Class Ksiegarnia.Sprzedawca Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, Ksiegarnia.Osoba)
{

Property identyfikator As %Integer [ Required ];

Relationship sprzedaz As Ksiegarnia.Sprzedaz [ Cardinality = many, Inverse = sprzedawca ];

Method przedstawSie() As %String
{
  Set dane = "[Sprzedawca "_..identyfikator_"] "_##super()
  return dane
}

}
