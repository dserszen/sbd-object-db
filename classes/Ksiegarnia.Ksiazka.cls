Class Ksiegarnia.Ksiazka Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor)
{

Property isbn As %String [ Required ];

Property tytul As %String [ Required ];

Property cena As %Double [ Required ];

Property sprzedana As %Boolean [ Required ];

Property kategoria As Ksiegarnia.KategoriaKsiazki [ Required ];

Property sprzedaz As Ksiegarnia.Sprzedaz;

Relationship autor As Ksiegarnia.Autor [ Cardinality = one, Inverse = ksiazki ];

Method czySprzedana() As %String
{
  IF ..sprzedana {
    Return "Tak"
  } ELSE {
    return "Nie"
  }
}

Method sprzedaj(ByRef sprzedawca As Sprzedawca, ByRef klient As Klient) As %String
{
  if ..sprzedana {
    Return "Nie mozna sprzedac ksiazki - jest juz sprzedana!"
  } else {
	Set spr = ##class(Ksiegarnia.Sprzedaz).%New()
    Set spr.ksiazka = $this
    Set spr.sprzedawca = sprzedawca
    Set spr.klient = klient
    Set spr.sprzedazAnulowana = 0
    Set spr.dataSprzedazy = ##class(%Library.UTC).NowUTC()
    Do spr.%Save()
    Set ..sprzedana = 1
	Set ..sprzedaz = spr;
    Do %Save()
    Return "Ksiazka zostala sprzedana"
  }
}

}
