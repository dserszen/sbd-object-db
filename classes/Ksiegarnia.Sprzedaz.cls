Class Ksiegarnia.Sprzedaz Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor)
{

Property dataSprzedazy As %TimeStamp [ Required ];

Property sprzedazAnulowana As %Boolean [ Required ];

Property ksiazka As Ksiegarnia.Ksiazka [Required ];

Relationship klient As Ksiegarnia.Klient [ Cardinality = one, Inverse = zakupy ];

Relationship sprzedawca As Ksiegarnia.Sprzedawca [ Cardinality = one, Inverse = sprzedaz ];

Index klientIndex On klient;

Index sprzedawcaIndex On sprzedawca;

Method anulujSprzedaz() As %String
{

  IF ..sprzedazAnulowana {
    Return "Tej sprzedazy nie mozna juz anulowac!"
  }
  ELSE{
    Set ..ksiazka.sprzedana = 0
    Do ..ksiazka.%Save()
    Set ..sprzedazAnulowana = 1
    Do %Save()
    Return "Sprzedaz zostala anulowana"
  }
}

}
