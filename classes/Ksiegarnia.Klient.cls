Class Ksiegarnia.Klient Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, Ksiegarnia.Osoba)
{

Property identyfikator As %Integer [ Required ];

Relationship zakupy As Ksiegarnia.Sprzedaz [ Cardinality = many, Inverse = klient ];

Method przedstawSie() As %String
{
  Set dane = "[Klient "_..identyfikator_"] "_##super()
  return dane
}

}
