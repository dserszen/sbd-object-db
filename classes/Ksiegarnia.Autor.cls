Class Ksiegarnia.Autor Extends (%Persistent, %Populate, %XML.Adaptor, %ZEN.DataModel.Adaptor, Ksiegarnia.Osoba)
{

Relationship ksiazki As Ksiegarnia.Ksiazka [ Cardinality = many, Inverse = autor ];

Method przedstawSie() As %String
{
  Set dane = "[Autor] "_##super()
  return dane
}
}
